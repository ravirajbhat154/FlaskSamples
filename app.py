from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template
app = Flask(__name__)

@app.route('/')
def home():
    return sampleActionMethod()


@app.route('/sampleActionMethod', methods=['GET', 'POST'])
def sampleActionMethod():
    if request.method == 'POST':
        return render_template('sampleTemplate.html',pagestatus1="active")
    else:
        return render_template('sampleTemplate.html',pagestatus1="active")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
